package predpal;

public class Pos {

	private int posicion;
	private int subPosicion;		// Para LangTool
	private String posTag;
	private String token;
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Pos(int posicion, int subPosicion, String posTag, String token) {
//		super();
		this.posicion = posicion;
		this.subPosicion = subPosicion;
		this.posTag = posTag;
		this.token = token;
	}

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	public int getSubPosicion() {
		return subPosicion;
	}

	public void setSubPosicion(int subPosicion) {
		this.subPosicion = subPosicion;
	}

	public String getPosTag() {
		return posTag;
	}

	public void setPosTag(String posTag) {
		this.posTag = posTag;
	}	
}