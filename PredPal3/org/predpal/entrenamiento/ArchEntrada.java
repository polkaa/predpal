package predpal.entrenamiento;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Pablo
 *
 */
public class ArchEntrada {

	private String archEntrada;
	private String categoria;
	private BufferedReader bufferedReader;
	
	/**
	 * @param archEntrada
	 * @param categoria
	 * @throws IOException
	 */
	public ArchEntrada(String archEntrada, String categoria) {
		this.archEntrada = archEntrada;
		this.categoria = categoria;
	}

	/**
	 * @return
	 * @throws IOException
	 */
	public BufferedReader abrirArchivo() throws IOException { 
		FileInputStream inputStream = new FileInputStream(archEntrada);
		
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF8");
		
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		
		this.bufferedReader = bufferedReader;
		
		return bufferedReader;
	}

	/**
	 * @throws IOException
	 */
	public void cerrarArchivo() throws IOException {
		bufferedReader.close();		
	}

	/**
	 * @return
	 */
	public BufferedReader getBufferedReader() {
		return bufferedReader;
	}

	public String getArchEntrada() {
		return archEntrada;
	}

	public String getCategoria() {
		return categoria;
	}
}