package predpal.entrenamiento;

import java.lang.Object;

public class Ngram {

	private String categoria = null;
	private String token = null;
	private String palabra = null;
	private int cantidad = 0;
	
	/**
	 * @param categoria
	 * @param token
	 * @param palabra
	 */
	public Ngram(String categoria, String token, String palabra) {
		this.categoria = categoria;
		this.token = token;
		this.palabra = palabra;

		cantidad = 1;
	}

	/**
	 * 
	 */
	public void agregarOcurrencia() {
		cantidad++;
	}

	/**
	 * @return
	 */
	public String getCategoria() {
		return categoria;
	}
	
	/**
	 * @return
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @return
	 */
	public String getPalabra() {
		return palabra;
	}
		
	/**
	 * @return
	 */
	public int getCantidad() {
		return cantidad;
	}
	
	/**
	 * @param cant
	 */
	public void setCantidad(int cant) {
		cantidad = cant;
	}
	
	@Override
    public int hashCode() {

		final int prime = 31;
		int result = 1;

		result = prime * result + ((categoria == null) ? 0 : categoria.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		result = prime * result + ((palabra == null) ? 0 : palabra.hashCode());

		return result;
    }

    @Override
    public boolean equals(Object object)
    {
        boolean sameSame = false;

        
        if (this == object) {
        	return true;
        }

        if (object == null || this.getClass() != object.getClass()) {
        	return false;
        }

        if (object != null && object instanceof Ngram)
        {
            Ngram reg = (Ngram) object;

            sameSame = ( reg.categoria != null && this.categoria != null && reg.categoria.equals(this.categoria) ||
            		   ( reg.categoria == null && this.categoria == null ) )
            		   &&
            		   ( reg.token != null && this.token != null && reg.token.equals(this.token) ||
            	       ( reg.token == null && this.token == null ) )
            		   &&
            		   ( reg.palabra != null && this.palabra != null && reg.palabra.equals(this.palabra) ||
             	       ( reg.palabra == null && this.palabra == null ) );
        }
        return sameSame;
    }
}