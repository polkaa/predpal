package predpal.entrenamiento;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class Tokenizer {

	private List<String> tokens;
	private BufferedReader archEntrada;
	
	/**
	 * @param archEntrada
	 */
	public Tokenizer(BufferedReader archEntrada) {
		this.archEntrada = archEntrada;
	}
	
	public Tokenizer() {
		
	}
	
	/**
	 * @return
	 * @throws IOException
	 */
	public List<String> generarTokens() throws IOException {
		
//		StreamTokenizer streamTok = new StreamTokenizer(archEntrada);
		List<String> tokensIniciales;

		
//		Tokens iniciales (separación por espacio)			
		tokensIniciales = obtenerTokens(archEntrada);		

		tokens = new ArrayList<String>();		
		
		this.tokens = sacarSimbolos(tokensIniciales);
		return tokens;
	}
	
	private boolean soloLetras(String palabra) {
		return palabra.chars().allMatch(Character::isLetter);		
	}		
		
	/**
	 * @return
	 */
	public List<String> getTokens() {
		return tokens;
	}
	
	private List<String> obtenerTokens(BufferedReader archEntrada) {
		String palabras[];		
		String line = null;
		List<String> tokens = new ArrayList<String>();
		
		
		try {
		
			while ((line = archEntrada.readLine()) != null) {

				palabras = line.split("\\s");

				for (int i=0; i<palabras.length; i++)
					tokens.add(palabras[i]);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}			
		
		return tokens;
	}
		
	private String obtenerPalabra(String palabra) {

		String palabraOut="";
		Reader reader;
	 	StreamTokenizer tokenizer;			
		boolean eof = false;
        int token;
		
		
		reader = new StringReader(palabra);
		tokenizer = new StreamTokenizer(reader);				
		
		try {
		
	    do {
			
			token = tokenizer.nextToken();

	        switch (token) {
	           case StreamTokenizer.TT_EOF:
	              eof = true;
	              break;
	           case StreamTokenizer.TT_EOL:
	              break;
	           case StreamTokenizer.TT_WORD:	        	   
	        	  palabraOut = (palabra.contains("$")) ? palabra : tokenizer.sval;  
	              break;	              
	           case StreamTokenizer.TT_NUMBER:
	        	  palabraOut = palabra;
	              break;
	              
	           default:
//				  Esto se debería tipear
	        	  if (tokenizer.sval != null)
	        		  palabraOut = tokenizer.sval;
	        	   	        	   
	              if (token == '!')
	                 eof = true;
	              
	              break;
	        }
	        if (palabraOut.length() > 0)
	        	break;
	        
	     } while (!eof);		
		} catch (IOException e) {
			e.printStackTrace();
		}
		return palabraOut;
	}

	private boolean tieneNumero(String palabra) {

//		Primero ver si tiene algún numero. En ese caso se debe tipear toda
		return palabra.chars().anyMatch(Character::isDigit);
	}
	
	public List<String> sacarSimbolos(List<String> tokensIniciales) {
		String separa[];
		List<String> tokens = new ArrayList<String>();
		String nuevo = "";
			
//		Recorrer cada token inicial
		for ( String palabra : tokensIniciales ) {
		
			if (tieneNumero(palabra)) {
				if (palabra.contains(".")) {
				
//					Si el punto está al final del número (es puntuación y no decimales)					
					if (palabra.lastIndexOf(".") == (palabra.length() -1) ) {
						
						separa = palabra.split("\\.");
						
						for ( String pal : separa) {
							if (!(pal.contains(".")))
								nuevo = nuevo + pal;

						}											
						tokens.add(nuevo);						
						tokens.add(".");
						nuevo = "";
						continue;
					}			
				} else if (palabra.contains(",")) {					
//					Para el caso: "Los '80,"					
					palabra = palabra.replaceAll(",", "");
				}
				
			} else if (!soloLetras(palabra)) {
//				Devuelve solo la palabra, sin comas, puntos, etc.
				palabra = obtenerPalabra(palabra);				
				
//				Si despues sigue con el punto, poner el punto como un nuevo token
				if (palabra.contains(".")) {

					separa = palabra.split("\\.");
					
					for ( String pal : separa) {
						if (!(pal.contains(".")))
							tokens.add(pal);
					}
					tokens.add(".");
					continue;
				}				
				
			}			

//			Devuelve true si palabra sólo contiene letras 				
			tokens.add(palabra);
		}		
		
		return tokens;
	}
	
	public List<String> obtenerTokens(String oracion) {
		List<String> tokens = new ArrayList<String>();
		String tokensIniciales[] = oracion.split("\\s");
		
		for ( String token : tokensIniciales)
			tokens.add(token);

		return tokens;
	}
} 