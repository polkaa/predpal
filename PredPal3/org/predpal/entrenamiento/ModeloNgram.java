package predpal.entrenamiento;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import predpal.entrenamiento.Ngram;
import predpal.util.DBUtil;

public class ModeloNgram {

	private Map<Ngram, Ngram> nGramas;
	private String categoria;
	
	public ModeloNgram(String categoria) {
		nGramas = new HashMap<Ngram,Ngram>();
		this.categoria = categoria;
	}
	
	/**
	 * @param categoria
	 * @param tokens
	 * @param nGramSize
	 */
	public void generarNgramas(String categoria, List<String> tokens, int nGramSize) {

		String token = "",
			   palabra = null;
		
	
		for (int i=0; i<(tokens.size()); i++) {

			for (int j=0; j<nGramSize; j++) {

//				Antes de acceder a la lista de tokens, verificar que el índice cae dentro del rango de la lista
				if (!(dentroLimite(i+j, tokens))) {
					break;
				}

				if (tokens.get(i+j).isEmpty()) {
					break;
				}
				
				palabra = tokens.get(i+j);
				
//				Generar token de acuerdo a nivel de N-gram (j)
				for (int y=j; y>0; y--) {
					if ( token == null ) {
						token = tokens.get((i+y)-1);
					} else {
						token = tokens.get((i+y)-1) + " " + token;
					}
				}

//				System.out.printf("token %s: %s Palabra: %s\n", j, token, palabra);
				token = token.trim();
				
//				Se generan N gramas que contienen el punto. Estos no se deben almacenar.				
				if (nGramaValido(token, palabra))				
					agregarNGrama(categoria, token, palabra);				

				token = "";
			}
		}
	}	

	/**
	 * @param categoria
	 * @param token
	 * @param palabra
	 */
	private void agregarNGrama(String categoria, String token, String palabra) {
		Ngram nGram;

		
		if (token == null)
			return;

		nGram = new Ngram(categoria, token, palabra);

		if (nGramas.containsKey(nGram)) {

//			Si ya existe el registro en el hashMap, actualizar cant. ocurrencias
			nGram.setCantidad(nGramas.get(nGram).getCantidad()+1);
			nGramas.remove(nGram);
			nGramas.put(nGram, nGram);

		} else
			nGramas.put(nGram, nGram);
	}

	public void guardarNGramas(String categoria) {

		int cant = 0;
		String sql = null;
		String values = null;
		String token = null;
		String palabra = null;
		Ngram nGram = null;
		Statement statement = null;
		int count = 0;
		DBUtil db;
		
		if (!existeCategoria(categoria))
			crearCategoria(categoria);
	
		try {
		
			db = new DBUtil();
			
			for (Map.Entry<Ngram, Ngram> entry : nGramas.entrySet()) {
	
				// Obtener el objeto registro
				nGram = entry.getValue();
				
				cant = getCantReg(nGram, db);
	
				token = nGram.getToken();
				palabra = nGram.getPalabra();				
				
//				Tener en cuenta comilla simple en el texto 
				token = token.replaceAll("'", "''");
				palabra = palabra.replaceAll("'", "''");					
				
				if (cant >= 1) {
	
					//sumar a la cantidad encontrada en base de datos
					cant = cant + nGram.getCantidad();
	
					sql = "update ngram " +
							"set cantidad = " + cant + " " +
							"where categoria = '" + categoria + "' and " +
							"token = '" + token + "' and " +
							"palabra = '" + palabra + "'";
	
				} else {

					values = "'" + categoria + "', " +
							"'" + token + "','" +	
							palabra + "'," +		
							nGram.getCantidad();					// cant
	
					//Si no encontró el registro agregarlo con cantidad de ocurrencias = 1
					sql = "insert into ngram values(" + values + ")";
				}
	
	//			try {
					
					if (statement == null ) {
						statement = db.getConnection().createStatement();
					}
	
					statement.addBatch(sql);
					
					if(++count % db.getBatchSize() == 0) {
	
						statement.executeBatch();	
						statement.clearBatch();
						statement.close();
						
						if (count % 10000 == 0) {} 
	//						log.setText("Por favor espere ... " + new DecimalFormat("##.##").format(  (  (double) count / hashMap.size() )*100) + "% Completado.");
							System.out.println("Por favor espere ... " + new DecimalFormat("##").format(  (  (double) count / nGramas.size() )*100) + "% Completado.\n");
	//						System.out.println(count);
					}								
	//			}
	//			catch (SQLException e) {	
	//				e.printStackTrace();		
	//			}						
			}	
//		try {
			System.out.println("Registros ingresados entrenamiento: " + count);

			if (statement == null ) {
				statement = db.getConnection().createStatement();
			}			
			statement.addBatch("COMMIT");	
			statement.executeBatch();			
			statement.clearBatch();			
			statement.close();
			db.closeDatabase();
			
		} catch (SQLException e) {	
			e.printStackTrace();
		}
//		db.closeDatabase();
	}	
	
	/**
	 * @param nGram
	 * @return
	 */
	private int getCantReg(Ngram nGram, DBUtil db) {
		
		ResultSet rs;
		String sql = null;
		String token;
		String palabra;
		Statement statement = null;
		int count = 0;		
		
		
		token = nGram.getToken();
		palabra = nGram.getPalabra();

		token = token.replaceAll("'", "''");
		palabra = palabra.replaceAll("'", "''");

		sql = "select cantidad from ngram " +
				"where categoria = '" + nGram.getCategoria() + "' and " + 
				"token = '" + token + "' and " +
				"palabra = '" + palabra + "'";		
		
		try {				

			if ( statement == null )
				statement = db.getConnection().createStatement();

			rs = statement.executeQuery(sql);

			while (rs.next()) {
				count = rs.getInt("cantidad");
				break;
			}		
			rs.close();
			statement.close();
		}
		catch (SQLException e) {	
			e.printStackTrace();
		}

		return count;
	}	

	/**
	 * @param index
	 * @param tokens
	 * @return
	 */
	private boolean dentroLimite(int index, List<String> tokens) {
		return (index >= 0) && (index < tokens.size());
	}
	
	public Map<Ngram, Ngram> getNGramas() {
		return nGramas;
	}	
		
	public boolean existeCategoria(String categoria) {
//		Database db = null;
		DBUtil db;
		boolean existe = false;
		ResultSet rs;
		String sql = null;
		Statement statement;
		
		
		sql = "select categoria from categorias " +
			  "where categoria = '" + categoria + "'";
		
		try {
			db = new DBUtil();
			statement = db.getConnection().createStatement();

			rs = statement.executeQuery(sql);

			if (!rs.isBeforeFirst() )		// No se encontraron datos
				existe = false;
			else
				existe = true;
			
			rs.close();
			statement.close();
			db.closeDatabase();
		}
		catch (SQLException e) {	
			e.printStackTrace();
		}	
		
		return existe;
	}
	
	public List<String> obtenerCategorias() {
		
		List<String> categorias = new ArrayList<String>();
//		Database db = null;
		DBUtil db;
		ResultSet rs;
		String sql = null;
		Statement statement;
	
		
		sql = "select categoria from categorias;";		
		
		try {
			
			db = new DBUtil();
			
			statement = db.getConnection().createStatement();

			rs = statement.executeQuery(sql);

			while (rs.next()) {
				categorias.add(rs.getString("categoria"));
			}		
			rs.close();
			statement.close();
			db.closeDatabase();
		}
		catch (SQLException e) {	
			e.printStackTrace();
		}
		
		return categorias;
	}
	
	private void crearCategoria(String categoria) {
		DBUtil db;
		String sql = null;
		Statement statement;
	
		
		sql = "insert into categorias values('" + categoria + "')";		
		
		try {
			db = new DBUtil();
			statement = db.getConnection().createStatement();
			statement.executeUpdate(sql);
			db.getConnection().commit();
			statement.close();
			db.closeDatabase();
		}
		catch (SQLException e) {	
			e.printStackTrace();
		}
	}
	
	public void borrarCategoria(String categoria) {
//		Database db = null;
		DBUtil db;
		Statement statement;
		String sql;
		
			
		try {
			db = new DBUtil();			
			statement = db.getConnection().createStatement();
			statement.setQueryTimeout(30);  // set timeout to 30 sec.

			sql = "DELETE FROM ngram ";
			sql = sql + "WHERE categoria = '" + categoria + "'";				

			statement.executeUpdate(sql);
			db.getConnection().commit();
			
			sql = "DELETE FROM categorias ";
			sql = sql + "WHERE categoria = '" + categoria + "'";

			statement.executeUpdate(sql);
			db.getConnection().commit();
			
			statement.close();
			
			db.closeDatabase();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}						
	}

	public String getCategoria() {
		return categoria;
	}

	public boolean nGramaValido(String token, String palabra) {
		String separar[];
		
//		Palabra sólo contiene un token		
		if (palabra.length() == 1 && palabra.contains(".")) {
			return false;
		}
		
//		token es la concatenación de dos tokens: Ej.: "2013 ."  [2013, .]
		if (token.contains(".")) {
//			Para el caso: ".|CAR"  ó ".|200.000,00"	 (Token es la parte a la izquierda de "|")	
			if (token.length() == 1)
				return false;
				
//			Para el caso: "2013 ."  ó ". 200.000,00|pesos" (Notar que hay un espacio entre el punto y el otro token)
//			Este caso especial tambien debe devolver False, Pero hay que tener en cuenta el caso que sea un nro.:
//			Ej.: "200.000,00|pesos"	--> Este caso debe ser true (ngrama válido)			
			
//			Ver si está al principio o al final del token para descartarlo			
			if (token.indexOf(".") == 0 || token.lastIndexOf(".") == token.length()-1)
				return false;
		}	

		return true;
	}
}