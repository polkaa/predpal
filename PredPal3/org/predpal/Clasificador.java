package predpal;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.lm.NGramProcessLM;
import com.aliasi.util.Files;
import com.aliasi.classify.JointClassification;
import com.aliasi.classify.LMClassifier;
import com.aliasi.util.AbstractExternalizable;
import predpal.entrenamiento.ArchEntrada;
import predpal.util.Config;

/**
 * @author Pablo
 *
 */
public class Clasificador {

    private static int NGRAM_SIZE = 6;
    private static String nomClasificador = Config.getProperty("ARCHIVO_CLASIFICADOR");
	private static String categorias[];
	private static List<ArchEntrada> datosEntrenamiento = new ArrayList<ArchEntrada>();    
	private static LMClassifier<?,?> clasificadorCompilado;
	
	public Clasificador() {
		try {
			cargar();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}

	public static void entrenar(String path) throws IOException, ClassNotFoundException {		

        DynamicLMClassifier<NGramProcessLM> clasificador;
        

//		A partir de un directorio obtener un array con las categorías a entrenar  
//		Este metodo tambien arma una lista con los datos de Entrenamiento        
        obtenerCategorias(path);

//		Crear objeto clasificador        
        clasificador = DynamicLMClassifier.createNGramProcess(categorias, NGRAM_SIZE);

//		Con los datos de entrenamiento, entrenar el clasificador
        for (ArchEntrada archivo : datosEntrenamiento) {

        	File arch = new File(archivo.getArchEntrada());
        	String texto = Files.readFromFile(arch,"UTF-8");

        	Classification clasificacion = new Classification(archivo.getCategoria());
        	Classified<CharSequence> clasificado = new Classified<CharSequence>(texto,clasificacion);
        	clasificador.handle(clasificado);        	        	
        }
        guardar(clasificador);        
//        for(int i = 0; i < categorias.length; ++i) {
//
//            File classDir = new File(path,categorias[i]);
//
//            if (!classDir.isDirectory()) {
//                String msg = "No se encontró el directorio de entrenamiento: "
//                    + classDir;
//                System.out.println(msg);
//                throw new IllegalArgumentException(msg);
//            }
//
//            String[] archivosEntrenamiento = classDir.list();
//            System.out.println("Entrenando con " + categorias[i]);
//            for (int j = 0; j < archivosEntrenamiento.length; ++j) {
//
//            	File archivo = new File(classDir,archivosEntrenamiento[j]);
//                String texto = Files.readFromFile(archivo,"ISO-8859-1");
//
//                Classification clasificacion = new Classification(categorias[i]);
//                Classified<CharSequence> clasificado = new Classified<CharSequence>(texto,clasificacion);
//                clasificador.handle(clasificado);
//            }
//        }         
        
	}
	
    private static void guardar(DynamicLMClassifier<NGramProcessLM> clasificador) throws ClassNotFoundException, IOException {    	
	    	
//		(Compila el clasificador : classifier -> compiledClassifier)
        File archivoClasificador = new File(nomClasificador);
        AbstractExternalizable.compileTo(clasificador, archivoClasificador); 
	}	

    public static LMClassifier<?,?> cargar() throws ClassNotFoundException, IOException {
	    	 
    	File archivoClasificador = new File(nomClasificador);
//    	LMClassifier<?,?> clasificadorCompilado;
    	
    	
    	clasificadorCompilado = (LMClassifier<?, ?>)AbstractExternalizable.readObject(archivoClasificador);
    	return clasificadorCompilado;
	}
	
//    public static String obtenerCategoria(JointClassifier<CharSequence> clasificadorCompilado, String texto) throws IOException {
    public static String obtenerCategoria(String texto) {    	
                      
        JointClassification jc = clasificadorCompilado.classify(texto);
        String mejorCategoria = jc.bestCategory();

        return mejorCategoria;
    }

	public static String[] obtenerCategorias(String path) {
		
		File dirEntrenamiento; 
		int cantCategorias = 0;
		
		
		dirEntrenamiento = new File(path);		
		
//		Obtener la cantidad de categorías (cantidad de directorios)		
		cantCategorias = Integer.valueOf(Long.toString(dirEntrenamiento.list().length));
				
		categorias = new String[cantCategorias];
	
//		Por cada categoría:		
		for (int i=0; i<dirEntrenamiento.list().length; i++) {
//			Guardar la categoría en el array 'categorias'			
			categorias[i] = dirEntrenamiento.list()[i];
        	
//			Este archivo representa el directorio de la categoria donde están los arch. para entrenar			
        	File dirCategoria = new File(dirEntrenamiento,categorias[i]);

            String archivosEntrenamiento[] = dirCategoria.list();
            for (int k = 0; k < archivosEntrenamiento.length; k++) {
//				Este archivo representa cada archivo de entrenamiento            	
            	File archivoEntrenamiento = new File(dirCategoria.getAbsolutePath(),archivosEntrenamiento[k]);
//				Se genera un objeto "archivo de entrada"            	
            	ArchEntrada archivo = new ArchEntrada(archivoEntrenamiento.getAbsoluteFile().toString(), categorias[i]);
//				Se guarda el archivo en una lista            	
            	datosEntrenamiento.add(archivo);
            }
		}
		
//		List array:
		for ( ArchEntrada archEnt : getDatosEntrenamiento()) {
			System.out.println(archEnt.getCategoria() + " - " + archEnt.getArchEntrada()); 
		}
			
		return categorias;					
	}

	public static int getNGRAM_SIZE() {
		return NGRAM_SIZE;
	}

	public static String getNomClasificador() {
		return nomClasificador;
	}

	public static String[] getCategorias() {
		return categorias;
	}

	public static List<ArchEntrada> getDatosEntrenamiento() {
		return datosEntrenamiento;
	}	
}