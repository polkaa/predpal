/**
 * @author Pablo
 *
 */
package predpal;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import predpal.util.Config;

public class CoreNLP {

	private StanfordCoreNLP pipeline;

	/**
	 * Constructor
	 */
	public CoreNLP() {

		Properties props = new Properties();

		props.setProperty("annotators", "tokenize, ssplit, pos");

//		Tokenize using Spanish settings
		props.setProperty("tokenize.language", "es");

//		Load the Spanish POS tagger model (rather than the default English model)
		props.setProperty("pos.model", "edu/stanford/nlp/models/pos-tagger/spanish/spanish-distsim.tagger");

//		Create a CoreNLP pipeline
		pipeline = new StanfordCoreNLP(props);
	}
	
	/**
	 * @param texto
	 * @return
	 */
//	public String obtenerUltimaOracion(String texto) {
//
//		Annotation annotation;
//		
//		annotation = new Annotation(texto);
//
////		Ejecutar los "anotadores" seleccionados sobre el texto
//		pipeline.annotate(annotation);
//
//		// Estas son las oraciones que existen en el texto
//		List<CoreMap> oraciones = annotation.get(CoreAnnotations.SentencesAnnotation.class);
//
//		if (oraciones.size() <= 0) {				// Si no hay oraciones
//			this.ultimaOracion = null;
//			return null;
//		} else {
//			this.ultimaOracion = oraciones.get(oraciones.size()-1).toString(); 
//			return this.ultimaOracion;
//		}
//	}

	/**
	 * @param texto
	 * @return
	 */
//	public CoreMap getUltimaOracionCoreMap(String texto) {
//		Annotation annotation;
//		
//		annotation = new Annotation(texto);
//		
////		Ejecutar los "anotadores" seleccionados sobre el texto
//		pipeline.annotate(annotation);
//
//		// Estas son las oraciones que existen en el texto
//		List<CoreMap> oraciones = annotation.get(CoreAnnotations.SentencesAnnotation.class);
//	
//		if (oraciones.size() <= 0)					  // Si no hay oraciones					
//			return null;							  
//		else {
//			this.ultimaOracion = oraciones.get(oraciones.size()-1).toString();
//			return oraciones.get(oraciones.size()-1); // Devolver la última oración
//		}
//	}
	
	/**
	 * @param texto
	 * @return
	 */
//	public List<String> obtenerUltimasNPalabras(String texto, byte nGram) {
//		CoreMap oracion;
//		List<String> ultPal = new ArrayList<String>();
//
//		
//		oracion = getUltimaOracionCoreMap(texto);
//		
//		if (oracion == null)
//			return ultPal;
//					
////		Recorrer las palabras de la oración
////		CoreLabel es un CoreMap con métodos adicionales específicos de tokens
//		String tokens[] = oracion.toString().split("\\s");
//		
//		for (String palabra : tokens) {
//			ultPal.add(palabra);
//		}
//		
////		for (CoreLabel token: oracion.get(CoreAnnotations.TokensAnnotation.class)) {
//////			Este es el texto del token
////			String word = token.get(CoreAnnotations.TextAnnotation.class);
////
//////			System.out.println(word + " - " + token.index() + "\n");
////			ultPal.add(word);
////
////		}
//
////		Devolver las ultimas N palabras de la oración (dado por nGram)	
//		return ultPal.subList(Math.max(ultPal.size() - nGram, 0), ultPal.size()); 
//	}

//	public String getUltimaOracion() {				
//		return ultimaOracion;
//	}
	
	public List<Pos> obtenerPosTags(String texto) {
		
		List<Pos> posTags = new ArrayList<Pos>();
		Annotation annotation;
	    
		
		if (texto == null)
			return posTags;
	
		annotation = new Annotation(texto);
	
	    pipeline.annotate(annotation);
    
	    List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
    
		if (Boolean.valueOf(Config.getProperty("LOG"))) {
			System.out.println("---------------------------------");	      
		    System.out.println("Stanford core NLP:");
		    System.out.println("---------------------------------");
		}
	    
	    for (CoreMap sentence: sentences) {
	    	
	    // traversing the words in the current sentence
	    // a CoreLabel is a CoreMap with additional token-specific methods
		    for (CoreLabel token: sentence.get(CoreAnnotations.TokensAnnotation.class)) {
		      // this is the text of the token
		      String word = token.get(CoreAnnotations.TextAnnotation.class);
	
		      // this is the POS tag of the token
		      String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
	
		      // this is the NER label of the token
		      String ne = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
		      
		      if (Boolean.valueOf(Config.getProperty("LOG")))		      
		    	  System.out.println(word + " - " + pos + " - " + token.index() + " - " + ne);
		      
		      Pos posReg = new Pos(token.index(), 0, pos, word);
		      posTags.add(posReg);		      
		    }
	    }
		return posTags;
	}		
}