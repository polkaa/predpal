/**
 *	PredPal - Clase Principal de acceso a la API de predicción
 *  @author Pablo
 */
package predpal;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import predpal.entrenamiento.ArchEntrada;
import predpal.util.*;

public class PredPal {

	private Gramatica gramatica;
	private Prediccion prediccion;
	private Texto textoEntrada;
	
	/**
	 * Constructor
	 */
	public PredPal() {

		gramatica = new Gramatica();
		prediccion = new Prediccion(this);
		prediccion.aprender();
	}

	public void entrenarClasificador(String path) {

		try {
			Clasificador.entrenar(path);
			
			for (ArchEntrada arch : Clasificador.getDatosEntrenamiento())		
				entrenarCategoria(arch.getArchEntrada(), arch.getCategoria());							
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param texto
	 * @return
	 */
	public List<String> predecir(String texto) {

		List<String> ultPal;
		List<String> palPred = new ArrayList<String>();
		List<String> palPredPersonal =	 new ArrayList<String>();


		gramatica.borrarLogReglas();
		
		if (texto.length() <= 0)
			return palPred;
			
//		Crear una instancia del objeto Texto que representa el texto de entrada
		textoEntrada = new Texto(texto);

//		Guardar el texto en el archivo de aprendizaje
		textoEntrada.guardar();
		
//		Obtener las últimas N palabras del texto
		ultPal = gramatica.obtenerUltimasPalabras(textoEntrada, obtenerNGram());
		
		if (obtenerCategoriaAutomatica())
			seleccionarCategoria(determinarCategoria(textoEntrada.getTextoEntrada()));

		prediccion.obtenerPrediccion(ultPal, textoEntrada, palPred, palPredPersonal);

		if (obtenerGramaticaActiva()) {
			palPred = gramatica.verificar(palPred, textoEntrada, 7);					
			palPredPersonal = gramatica.verificar(palPredPersonal, textoEntrada, 3);
		}
		
//		Combinar predicciones de Categoría + Diccionario Personal		
		palPred = combinar(palPred, palPredPersonal);
		
		return palPred; 
	}

	private List<String> combinar(List<String> palPred, List<String> palPredPersonal) {
		List<String> palabras = new ArrayList<String>();
		int count = 0;

//		Tomar 7 palabras predichas de la categoria seleccionada (o automatica) y 3 palabras
//		del diccionario personal
		palabras = palPred.subList(0, ( (palPred.size() < 7 ) ? palPred.size() : 7 ));

		for ( String palabra : palPredPersonal ) {
			if (!palabras.contains(palabra)) {
				palabras.add(palabra);
				count++;
				
				if (count >= 3)
					break;
			}
		}

		return palabras;
	}

	public void entrenarCategoria(String rutaArchivo, String categoria) {		
		prediccion.entrenar(rutaArchivo, categoria);		
	}

	public List<String> obtenerCategorias() {
		return prediccion.obtenerCategorias(); 
	}

	public void borrarCategoria(String categoria) {
		prediccion.borrarCategoria(categoria);
	}

	public List<String> obtenerMensajes() {
		return gramatica.getLogReglas();
	}

	public void seleccionarCategoria(String categoria) {
		Config.setProperty("CATEGORIA", categoria);
		Config.grabar();
	}	

	public void activarClasificador(boolean auto) {
		Config.setProperty("CATEGORIA_AUTO", String.valueOf(auto));
		Config.grabar();		
	}

	public boolean obtenerCategoriaAutomatica() {		
		return Boolean.valueOf(Config.getProperty("CATEGORIA_AUTO"));
	}
	
	public String determinarCategoria(String texto) {

		String categoria = "";

		try {
			Clasificador.cargar();
			categoria = Clasificador.obtenerCategoria(texto);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return categoria;
	}
	
	public String obtenerCategoria() {
		return Config.getProperty("CATEGORIA");
	}
		
	public boolean obtenerClasificador() {

		// Verificar si existe el archivo_learn.txt antes de entrenar:
		File archClasificador = new File(Config.getProperty("ARCHIVO_CLASIFICADOR"));

		return archClasificador.exists();
	}
	
	public void activarGramatica(boolean value) {
		Config.setProperty("GRAMATICA", String.valueOf(value));
		Config.grabar();				
	}
	
	public boolean obtenerGramaticaActiva() {
		return Boolean.valueOf(Config.getProperty("GRAMATICA"));		
	}
	
	public void activarCategoriaPersonal(boolean value) {
		Config.setProperty("CATEGORIA_PERSONAL", String.valueOf(value));
		Config.grabar();		
	}
	
	public boolean obtenerCategoriaPersonalActiva() {
		return Boolean.valueOf(Config.getProperty("CATEGORIA_PERSONAL"));		
	}
	
	public void seleccionarNGram(int value) {
		Config.setProperty("NGRAM", String.valueOf(value));
		Config.grabar();					
	}
	
	public int obtenerNGram() {
		return Integer.valueOf(Config.getProperty("NGRAM"));		
	}
	
	public void activarCoreNlp(boolean value) {
		Config.setProperty("CORENLP", String.valueOf(value));
		Config.grabar();
	}
}