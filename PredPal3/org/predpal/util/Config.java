package predpal.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {
	private static Properties defaultProps = new Properties();

	static {
		try {
			
			File archivoConfig = new File("config.txt");;
			
			if (!archivoConfig.exists()) {
				archivoConfig.createNewFile();
				construirPropiedades();
				grabar();
			}
							
			FileInputStream in = new FileInputStream(archivoConfig);
			
			defaultProps.load(in);
			in.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getProperty(String key) {
		return defaultProps.getProperty(key);
	}

	public static void setProperty(String key, String value) {
		defaultProps.setProperty(key, value);
	}

	public static void grabar() {
		FileOutputStream out;

		try {
			out = new FileOutputStream("config.txt");
			defaultProps.store(out, "---No Comment---");
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void construirPropiedades() {
		setProperty("NGRAM", "3");		
		setProperty("CANT_PALABRAS_PRED", "7");
		setProperty("CATEGORIA", "genero");
		setProperty("LIMITE_CONTEO", "4");
		setProperty("ARCHIVO_APRENDIZAJE", "aprendizaje.txt");
		setProperty("CATEGORIA_AUTO", "false");
		setProperty("GRAMATICA", "true");
		setProperty("CATEGORIA_PERSONAL", "true");
		setProperty("ARCHIVO_CLASIFICADOR", "clasificador.txt");		
		setProperty("LOG", "false");
		setProperty("CORENLP", "true");
	}
}