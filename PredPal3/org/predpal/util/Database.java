package predpal.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
 
public final class Database {

	private static Connection connection;
	private static Statement statement = null;
	private static final String DB_URL = "jdbc:sqlite:predPal.db";

	//	Para proceso batch	
	private static final int batchSize = 500;
	private static int count = 0;

	static {
		System.out.println("INIT");
		if (connection == null) {
			createConn();
		}
	}
	
	private static void createConn() {
		try {		

			Class.forName("org.sqlite.JDBC");

			// create a database connection
			connection = DriverManager.getConnection(DB_URL);

			// Verificar si hay que crear el esquema de base de datos			
			if (!tieneEsquema())
				crearEsquema();				

			connection.setAutoCommit(false);  // sin esta opción los INSERT demoran una eternidad

		} catch (ClassNotFoundException e) {
			e.printStackTrace();			
		} catch(SQLException e)	{
			// connection close failed.
			System.err.println(e);
		}		
		
	}
	
	private static boolean tieneEsquema() {

		Statement statement;
		String sql;
		boolean tieneEsquema = false;

		try {
			statement = connection.createStatement();
			statement.setQueryTimeout(30);  // set timeout to 30 sec.

			sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='ngram'";

			ResultSet rs = statement.executeQuery(sql);

			if (rs.next())
				tieneEsquema = true;
			else
				tieneEsquema = false;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return tieneEsquema;
	}

	private static void crearEsquema() {
		
		String sql = null;
		
	    try {
			Statement statement = connection.createStatement();
			
			sql = "create table ngram ("; 
			sql = sql + "categoria TEXT, token TEXT, palabra TEXT, cantidad integer, ";
			sql = sql + "PRIMARY KEY(categoria, token, palabra))";
//			System.out.println(sql);
//			Crear tabla Ngram			
			statement.executeUpdate(sql);
//			Crear tabla categorías			
			statement.executeUpdate("create table categorias (categoria TEXT, PRIMARY KEY(categoria))");
			
		    statement.close();			
		} catch (SQLException e) {
			e.printStackTrace();
		}	    
	}

	public static int getBatchSize() {
		return batchSize;
	}
	
	public static void closeDatabase() {
		
		try	{
			if ( connection != null )
				connection.close();
		} catch(SQLException e)	{
			// connection close failed.
			System.err.println(e);
			System.exit(-1);
		}				
	}
	
	public static Connection getConnection() {
		
		if (connection == null)
			createConn();
			

		return connection;
	}	
	
	public static ResultSet executeQuery(String sql) {

		ResultSet rs = null;
		Statement statement = null;

		try {
			statement = connection.createStatement();
			
			rs = statement.executeQuery(sql);						
			
		} catch (SQLException e) {	
			e.printStackTrace();		
		}
		return rs;	
	}

	public static int getCount(String sql) {

		int cant = 0;
		ResultSet rs = null;
		Statement statement = null;

		try {
			statement = connection.createStatement();

			rs = statement.executeQuery(sql);

			while(rs.next())
			    cant = rs.getInt("cant");
			
			rs.close();
			statement.close();
			
		} catch (SQLException e) {	
			e.printStackTrace();		
		}
		return cant+1;		
	}
	
	public static int executeUpdate(String sql) {		

		try {
			if (statement == null) {
				statement = connection.createStatement();
			}
			
			statement.addBatch(sql);
			
			if(++count % batchSize == 0) {
		        statement.executeBatch();
		        statement.close();				
		        count = 0;
		    }
			
//			Van a faltar los registros restantes despues del ultimo executeBatch
//			y que no llegan a mil			
			
		} catch (SQLException e) {

			String mensaje = e.getMessage();
			
			if (mensaje.contains("UNIQUE constraint failed")) {
				e.printStackTrace();				
				return 1;
			} else {
				e.printStackTrace();
			}
		}
		return 0;
	}
}