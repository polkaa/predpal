package predpal.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
 
public class DBUtil {
	
	private Connection connection = null;
	private final String DB_URL = "jdbc:sqlite:predPal.db";
	
//	Para proceso batch	
	private final int batchSize = 500;
	private int count = 0;
	
	private Statement statement = null;	
	
	public DBUtil() {

		try {		
			Class.forName("org.sqlite.JDBC");

			// create a database connection
			connection = DriverManager.getConnection(DB_URL);

//			Verificar si hay que crear el esquema de base de datos			
			if (!tieneEsquema())
				crearEsquema();				
			
			connection.setAutoCommit(false);  // sin esta opciÃ³n los INSERT demoran una eternidad

		} catch (ClassNotFoundException e) {
			e.printStackTrace();			
		} catch(SQLException e)	{
			// connection close failed.
			System.err.println(e);
		}					
	}

	private boolean tieneEsquema() {

		Statement statement;
		String sql;
		boolean tieneEsquema = false;

		try {
			statement = connection.createStatement();
			statement.setQueryTimeout(30);  // set timeout to 30 sec.

			sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='ngram'";

			ResultSet rs = statement.executeQuery(sql);

			if (rs.next())
				tieneEsquema = true;
			else
				tieneEsquema = false;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return tieneEsquema;
	}

	private void crearEsquema() {
		
		String sql = null;
		
	    try {
			Statement statement = connection.createStatement();
			
			sql = "create table ngram ("; 
			sql = sql + "categoria TEXT, token TEXT, palabra TEXT, cantidad integer, ";
			sql = sql + "PRIMARY KEY(categoria, token, palabra))";
//			System.out.println(sql);
//			Crear tabla Ngram			
			statement.executeUpdate(sql);
//			Crear tabla categorÃ­as			
			statement.executeUpdate("create table categorias (categoria TEXT, PRIMARY KEY(categoria))");
			
		    statement.close();			
		} catch (SQLException e) {
			e.printStackTrace();
		}	    
	}

	public int getBatchSize() {
		return batchSize;
	}
	
	public void closeDatabase() {
		
		try	{
			if ( connection != null )
				connection.close();
		} catch(SQLException e)	{
			// connection close failed.
			System.err.println(e);
			System.exit(-1);
		}				
	}
	
	public Connection getConnection() {
		return connection;
	}	
	
	public ResultSet ejecutarQuery(String sql) {

		ResultSet rs = null;
		Statement statement = null;

		try {
			statement = connection.createStatement();
			
			rs = statement.executeQuery(sql);						
			
		} catch (SQLException e) {	
			e.printStackTrace();		
		}
		return rs;	
	}

	public int getCount(String sql) {

		int cant = 0;
		ResultSet rs = null;
		Statement statement = null;

		try {
			statement = connection.createStatement();

			rs = statement.executeQuery(sql);

			while(rs.next())
			    cant = rs.getInt("cant");
			
			rs.close();
			statement.close();
			
		} catch (SQLException e) {	
			e.printStackTrace();		
		}
		return cant+1;		
	}
	
	public int ejecutarUpdate(String sql) {		

		try {
			if (statement == null) {
				statement = connection.createStatement();
			}
			
			statement.addBatch(sql);
			
			if(++count % batchSize == 0) {
		        statement.executeBatch();
		        statement.close();				
		        count = 0;
		    }
			
//			Van a faltar los registros restantes despues del ultimo executeBatch
//			y que no llegan a mil			
			
		} catch (SQLException e) {

			String mensaje = e.getMessage();
			
			if (mensaje.contains("UNIQUE constraint failed")) {
				e.printStackTrace();				
				return 1;
			} else {
				e.printStackTrace();
			}
		}
		return 0;
	}
}