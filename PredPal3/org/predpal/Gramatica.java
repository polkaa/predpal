/**
 * @author Pablo
 *
 */
package predpal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;

import predpal.entrenamiento.Tokenizer;
import predpal.util.Config;

public class Gramatica {

	private LangTool langTool;
	private CoreNLP coreNLP; 
	private ReglasGramaticales reglasGramaticales;	
	private List<String> ultPal;
	private String ultimaOracion;
	private List<String> logReglas = new ArrayList<String>();
	
	
	public Gramatica() {		
//		Crear instancia de librería Stanford CoreNLP		
		coreNLP = new CoreNLP();
		langTool = new LangTool();
		reglasGramaticales = new ReglasGramaticales();		
	}

	public List<String> verificar(List<String> palPred, Texto textoEntrada, int cantMax) {
		
		List<String> correctas = new ArrayList<String>();  
		String oracionAVerificar;
			

		if (palPred.isEmpty())
			return palPred;
		
//		"obtenerOracionBase" descarta la palabra parcial ingresada (si la hubiere)
		oracionAVerificar = obtenerOracionBase(getUltimaOracion(), textoEntrada.esParcial());		
		
		try {
			correctas = this.langTool.verificar(palPred, oracionAVerificar);

			if (Boolean.valueOf(Config.getProperty("CORENLP")))
//				LT no detecta algunos errores. Desambiguar y verificar con reglas Custom			
				correctas = verificarCorenNLP(correctas, oracionAVerificar, cantMax);
//
		} catch (IOException e) {
			e.printStackTrace();
		}			
//
		return correctas;
	}
	
	/**
	 * Obtiene las últimas palabras
	 * @param texto
	 * @return
	 */
	public List<String> obtenerUltimasPalabras(Texto textoEntrada, int nGram) {

		String oracion = "";
		Tokenizer tokenizer;
//		this.ultPal = coreNLP.obtenerUltimasNPalabras(textoEntrada.getTextoEntrada(), nGram);	
		
		List<String> ultPal = new ArrayList<String>();

		
		oracion = obtenerUltimaOracion(textoEntrada.getTextoEntrada());
		
		if (oracion == null)
			return ultPal;
					
		
		tokenizer = new Tokenizer();
//		Recorrer las palabras de la oración
//		CoreLabel es un CoreMap con métodos adicionales específicos de tokens
//		String tokens[] = oracion.toString().split("\\s");				
		
		ultPal = tokenizer.sacarSimbolos(tokenizer.obtenerTokens(oracion.toString()));		
		
//		for (String palabra : tokens)
//			ultPal.add(palabra);

//		Devolver las ultimas N palabras de la oración (dado por nGram)	
		this.ultPal = ultPal.subList(Math.max(ultPal.size() - nGram, 0), ultPal.size());
		
		return this.ultPal;
	}

	private String obtenerUltimaOracion(String texto) {
		String palabra = "";
		int index = 0;
		
		index = texto.lastIndexOf('.');
		
		if (index >= 0)
			palabra = texto.substring(index+1, texto.length());
		else
			palabra = texto;
		
		this.ultimaOracion = palabra.trim();;
		
		return this.ultimaOracion;			
	}
		
	public List<String> getUltimasPalabras() {
		return ultPal;
	}
	
	private String obtenerOracionBase(String ultimaOracion, boolean parcial) {

		String palabraParcial = null,
			   oracionAVerificar = null;
		

		if (parcial) {
//			Obtener la palabra parcial (ultima palabra de la lista ultPal)			
			palabraParcial = this.ultPal.get(ultPal.size()-1);
			
//			Oracion a verificar nunca tiene el %
			if (ultimaOracion.lastIndexOf(palabraParcial) > 0)
				oracionAVerificar = ultimaOracion.substring(0, Math.max((ultimaOracion.lastIndexOf(palabraParcial) - 1), 0));
			else
//	error :!:!!		esta devolviendo menos 1 en el 2do. parametro
				oracionAVerificar = ultimaOracion.substring(0, Math.max((ultimaOracion.lastIndexOf(palabraParcial)), 0));
 		
		} else
			oracionAVerificar = ultimaOracion;		
		
		return oracionAVerificar;
	}

	private List<Pos> desambiguar(List<Pos> posTagsNLP, List<Pos> posTagsLT) {
		
//		boolean foundTag = false;
		
//		---------------------------------
//		LangTool:
//		---------------------------------
//		Las - NCMP000 - 1 - 1
//		Las - PP3FPA00 - 2 - 2
//		Las - DA0FP0 - 3 - 3
//		Las - SENT_END - 4 - 4
//		Las - PARA_END - 5 - 5				

//		---------------------------------
//		Stanford core NLP:
//		---------------------------------
//		Las - da0000 - 1 - null
		
		List<Pos> posDesamb = new ArrayList<Pos>();
		Iterator<Pos> posAux = posTagsLT.iterator();

//		Como ya no estoy eliminando, puedo dejar de utilizar el iterator		
		
		while ( posAux.hasNext() ) {
		
//			foundTag = false;
			Pos posRegLT = posAux.next();
			
			for ( Pos posRegNLP : posTagsNLP ) {

//				Verificar misma palabra (token)				
				if (!(posRegLT.getToken().equals(posRegNLP.getToken())))
					continue;
				
//				Verificar Primer caracter del PosTAG:				
				if ( posRegLT.getPosTag().substring(0, 1).equals(posRegNLP.getPosTag().substring(0, 1).toUpperCase()) ) {
					
//					foundTag = true;
//					Si ya está no volver a agregarlo (misma posicion)
					if (!existe(posRegLT, posDesamb)) 										
						posDesamb.add(posRegLT);
					
					break;
				}
			}
//			---------------------------------
//			Stanford core NLP:
//			---------------------------------
//			A - sp000 - 1 - null
//			las - da0000 - 2 - null
//			seis - pn000000 - 3 - null
//			de - sp000 - 4 - null
//			. - fp - 5 - null
//			---------------------------------
//			LangTool:
//			---------------------------------
//			A - NCFS000 - 1 - 1
//			A - SPS00 - 2 - 2
//			las - NCMP000 - 1 - 1
//			las - PP3FPA00 - 2 - 2
//			las - DA0FP0 - 3 - 3
//			seis - Z - 1 - 1
//			de - NCFS000 - 1 - 1
//			de - SPS00 - 2 - 2
//			. - _PUNCT - 2 - 1
//			---------------------			
			
//			Al final del recorrido por un postag LT de número (Z), no va a encontrar par en CoreNLP
//			porque el código es PN
			if (posRegLT.getPosTag().substring(0, 1).equals("Z")) {
				
				for ( Pos posRegNLP : posTagsNLP ) {
					
					if (posRegNLP.getPosTag().substring(0, Math.min(posRegNLP.getPosTag().length(),2)).toUpperCase().equals("PN")) {
					
						if (!existe(posRegLT, posDesamb)) {
							
//							foundTag = true;
							posDesamb.add(posRegLT);
							break;
						}
					}
				}
			}
			
//			Este bloque produce una caída en el porc. de predicción
//			if (!foundTag) {
//				if (!existe(posRegLT, posDesamb))										
//					posDesamb.add(posRegLT);
//			}
//			foundTag = false;
		}	
				
		return posDesamb;
	}
	
	private boolean existe(Pos posRegLT, List<Pos> posDesamb) {
		
		boolean retVal = false;
		
		for ( Pos pos : posDesamb) {
			
			if (pos.getPosicion() == posRegLT.getPosicion()) {
				retVal = true;
				break;
			}
		}
		return retVal;
	}
	
	private boolean reglasCustom(List<Pos> posTagsLT) {

		List<String> logReglasAux = new ArrayList<String>();
		
		String oracionTags = null,
			   oracion = "";


		if (posTagsLT.isEmpty())
			return false;
		
		for ( Pos posReg : posTagsLT ) {

			if ( oracionTags == null) {
				oracionTags = posReg.getPosTag();
				oracion = posReg.getToken();
			}
			else {
				oracionTags = oracionTags + '¿' + posReg.getPosTag();
				oracion = oracion + " " + posReg.getToken();
			}
		}

		if (Boolean.valueOf(Config.getProperty("LOG"))) {
						
			System.out.println("---------------------");
			System.out.println("Tags oracion: " + oracionTags);
			System.out.println("Oración: " + oracion);
			System.out.println("---------------------");
		}
		
		for ( Regla regla : reglasGramaticales.getReglas() ) {
			
			Matcher m = regla.getPatron().matcher(oracionTags);
			
			if (m.matches()) {
//				Verificar excepciones

				for ( String excepcion : regla.getExcepciones() )
					if (oracion.contains(excepcion)) {
						if (Boolean.valueOf(Config.getProperty("LOG")))						
							System.out.println("Exception found");
						break;
					}
										
//				System.out.println("Start: " + m.start());
//				System.out.println("End: " + m.end());
//				System.out.println("Group: " + m.group());
//				System.out.println("Find: " + m.find());
				
				logReglasAux.add("Regla: " + regla.getIdRegla());
				logReglasAux.add(regla.getMensaje());
				logReglasAux.add(oracion);
				logReglasAux.add("--------------------------------------------------------------------------");				
			}
		}
		
		if (logReglasAux.isEmpty())
			return false;
		else {
			logReglas.addAll(logReglasAux);
			return true;
		}
	}
	
	private void eliminarSobrante(List<String> correctas) {
		
		int cant_palabras_pred;
		int count = 0;

		
		cant_palabras_pred = Integer.valueOf(Config.getProperty("CANT_PALABRAS_PRED"));
		
		if (correctas.size() <= cant_palabras_pred)
			return;
	
		Iterator<String> correct = correctas.iterator();				
						
		while (correct.hasNext()) {
			correct.next();
			count+=1;
			
			if (count > cant_palabras_pred)
				correct.remove();
		}
	}

	public List<String> getLogReglas() {
		
//		Agregar los mensajes de LangTool		
		logReglas.addAll(langTool.getLogReglas());
		return logReglas;
	}
	
	public String getUltimaOracion() {
		return ultimaOracion;
	}
	
	public void borrarLogReglas() {
		langTool.borrarLogReglas();
		this.logReglas.clear();
	}
	
	private List<String> verificarCorenNLP(List<String> correctas, String oracionAVerificar, int cantMax) {

		String correcta;
		String oracion;
		List<Pos> posTagsNLP;
		List<Pos> posTagsLT;			
		int count = 0;		

		
		if (correctas.isEmpty())
			return correctas;		
		
		List<String> correctasAux = new ArrayList<String>(correctas);
		
		Iterator<String> itCorrectas = correctasAux.iterator();				

		try {
			
			while (itCorrectas.hasNext() && count < cantMax) {
				correcta = itCorrectas.next();
	
				if (oracionAVerificar.length() == 0)
					oracion = correcta;
				else
					oracion = oracionAVerificar + " " + correcta;			
				
				if (!oracion.contains("."))
					oracion = oracion + '.';
				
				posTagsNLP = coreNLP.obtenerPosTags(oracion);					
				posTagsLT = langTool.obtenerPosTags(oracion, langTool.getLangTool());					
				
//				Desambiguar LangTool con Stanford Core NLP:
				posTagsLT = desambiguar(posTagsNLP, posTagsLT);				
	
				if (reglasCustom(posTagsLT))
					itCorrectas.remove();
				else
					count++;
				
			}					
		} catch (IOException e) {
			e.printStackTrace();
		}
		return correctasAux;		
	}
}