package predpal;

import java.util.List;
import java.util.regex.Pattern;

public class Regla {

	private Pattern patron;
	private String idRegla;
	private String mensaje;	
	private List<String> excepciones;
	
	public Regla(Pattern patron, String idRegla, String mensaje, List<String> excepciones) {
		this.patron = patron;
		this.idRegla = idRegla;
		this.mensaje = mensaje;
		this.excepciones = excepciones;
	}

	public Pattern getPatron() {
		return patron;
	}

	public void setPatron(Pattern patron) {
		this.patron = patron;
	}

	public String getIdRegla() {
		return idRegla;
	}

	public void setIdRegla(String idRegla) {
		this.idRegla = idRegla;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public List<String> getExcepciones() {
		return excepciones;
	}	
}