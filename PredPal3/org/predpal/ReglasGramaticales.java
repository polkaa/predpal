package predpal;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class ReglasGramaticales {

	private List<Regla> listaReglas = new ArrayList<Regla>();

	public ReglasGramaticales() {				
		cargarReglas();
	}

	public void cargarReglas() {

		Regla regla;
		List<String> excepciones = new ArrayList<String>();
				
//      -------------------------------------------------------------------------				
//		Esta regla aplicaria unicamente cuando el "verbo" es ser|parecer|estar.
//		Falta acotar los casos que entran en esta regla.. segun grammar.xml
//		"Las Casas son linda"
//		DA0FP0.NCFP000.VSIP3P0.AQ0CS0

		Pattern patron = Pattern.compile("D.{3}P.*¿N.{2}P.{3}¿V.{2}P.{3}¿A.{3}S.*");		
		regla = new Regla(patron, "SUJ_PRED_ATRIB_PLURAL_AUX", "Concordancia de número dodosa", excepciones);		
		listaReglas.add(regla);
//      -------------------------------------------------------------------------		
//		Esta regla aplicaria unicamente cuando el "verbo" es ser|parecer|estar.
//		Falta acotar los casos que entran en esta regla.. segun grammar.xml
//		"Las casas es azules"
//		DA0FS0¿NCFS000¿VSIP3S0¿AQ0CP0
		
		patron = Pattern.compile("D.{3}S.*¿N.{2}S.{3}¿V.{2}P.{3}¿A.{3}P.*");		
		regla = new Regla(patron, "SUJ_PRED_ATRIB_SINGULAR", "Concordancia de número dodosa", excepciones);		
		listaReglas.add(regla);
//      -------------------------------------------------------------------------		
//		Concordancia de género. Ejemplo: "La casa es lindo", "Las casas son hermosos"	
//		DA0FS0¿NCFS000¿VSIP3S0¿AQ0MS0				
		
		patron = Pattern.compile(".*¿N.{1}F.{4}¿V.{2}P.{3}¿A.{2}M.*");
		regla = new Regla(patron, "SUJ_PRED_ATRIB_FEM_AUX", "Concordancia de género dodosa", excepciones);		
		listaReglas.add(regla);		
//      -------------------------------------------------------------------------	
//		Esta regla aplicaria unicamente cuando el "verbo" es ser|parecer|estar.
//		"Los aviones son lindas" / "El avion es linda" (Es un error de género)
//		DA0MP0¿NCMP000¿VSIP3P0¿AQ0FP0		
		patron = Pattern.compile(".*¿N.{1}M.{4}¿V.{2}P.{3}¿A.{2}F.*");		
		regla = new Regla(patron, "SUJ_PRED_ATRIB_MASC_AUX", "Concordancia de género dodosa", excepciones);		
		listaReglas.add(regla);			
//      -------------------------------------------------------------------------		
//		Complemento Regla: DET_NOM_FOM
//		Concordancia de género dudosa
//		Posible falta de concordancia		
//		DA0FS0¿NCMS000
//		Ejemplo de error: "La contexto
//		Esta regla la está detectando LanguageTool (SERIA SUPERFLUA !!! ???)
		
		patron = Pattern.compile("DA0FS0¿NCMS000.*");
		regla = new Regla(patron, "DET_FEM_+_SUST_MASC", "Concordancia de género dodosa", excepciones);
		listaReglas.add(regla);				
//      -------------------------------------------------------------------------

//		Complemento Regla: Determinante singular + Adjetivo plural ó
//						   Determinante plural   + Adjetivo singular
//		Concordancia de número dudosa
//		Posible falta de concordancia		
//		DA0FS0¿AQ0FP0
//		Ejemplo de error: "La ciertas", "La correctas", El ciertos, El correctos

		patron = Pattern.compile("(D.{3}S0¿A.{3}P0.*)|(D.{3}P0¿A.{3}S0.*)");
		regla = new Regla(patron, "DET_SING_ADJ_PLU", "Concordancia de número dodosa", excepciones);
		listaReglas.add(regla);
//	    =============================================================================================
//		Regla: Determinante (singular|plural) + Pronombre (singular|plural) 
//		Ejemplo de error: "La cuales"
		
		
//		Vieja descripción:
//		Complemento Regla: Determinante femenino singular + Pronombre Plural ó
//						   Determinante femenino plural + Pronombre Singular			
//		Concordancia de número
//		Posible falta de concordancia		
//		DA0FS0¿PR0CP000
//		Ejemplo de error: "La cuales"

		
//		patron = Pattern.compile("(DA0FS0¿PR0CP000.*)|(DA0FP0¿P.{3}S000.*)");
		patron = Pattern.compile("(D.{3}S0¿P.{3}P000.*)|(D.{3}P0¿P.{3}S000.*)");
		regla = new Regla(patron, "DET_PRO_NUM", "Concordancia de número dodosa", excepciones);
		listaReglas.add(regla);
//	    =============================================================================================		
//		Complemento Regla: Sustantivo Singular + Adjetivo plural
//		Concordancia de número
//		Posible falta de concordancia
//		DA0FS0¿NCFS000¿AQ0CP0
//		DA0FS0¿NCFS000¿AQ0MP0

//		Ejemplo de error: // La casa existentes, La casa específicos 

		patron = Pattern.compile(".*¿NCFS000¿A.{3}P0.*");
		regla = new Regla(patron, "SUST_SING_ADJ_PLU", "Concordancia de número dodosa", excepciones);
		listaReglas.add(regla);
//	    -------------------------------------------------------------------------				
//		Complemento Regla: Excluir preprosiciones después de artículo a excepción de contra (Ej la con)
//		DA0FS0¿SPS00

//		Ejemplo de error: "La ante" 

		patron = Pattern.compile(".*DA.{4}¿SP.{3}.*");

//		Falso positivo: "El ventero que estaba más que eran acomodados ´de los de´"
//		´de los de´ podría ser: "Te llegó esto de los de arriba".		

//		Falso positivo: "El ventero que estaba más que eran acomodados ´de los a´"
//		´de los a´ podría ser: "Te llegó esto de los a priori ganadores".		
		
//		Falso positivo: "El ventero que estaba más que eran acomodados ´de los hasta´"
//			´de los hasta´ podría ser: "Te llegó esto de los hasta ahora primeros en la competencia".		
		
		
		excepciones = new ArrayList<String>();		
		excepciones.add("de los");
		regla = new Regla(patron, "ART_PRE", "Artículo seguido de preposición", excepciones);
		listaReglas.add(regla);
//		excepciones.clear();
//	    -------------------------------------------------------------------------				
//		Complemento Regla: Excluir verbo infinitivo después de sustantivo (ej la casa evaluar)
//		Ejemplo de error: "La casa evaluar"
				
		patron = Pattern.compile(".*N.{6}¿V.{1}N.{4}.*");
//		excepciones = new ArrayList<String>();		
		regla = new Regla(patron, "SUST_VERB_INF", "Sustantivo seguido de Verbo Infinitivo", excepciones);		
		listaReglas.add(regla);		
//	    -------------------------------------------------------------------------
//		Complemento Regla: Adjetivo Singular después de Sustantivo Plural
//		DA0FP0¿NCFP000¿AQ0FS0
//		Ejemplo de error: "Las casas estadística"
				
		patron = Pattern.compile(".*N.{2}P.{3}¿AQ.{2}S.{1}.*");
//		excepciones = new ArrayList<String>();		
		regla = new Regla(patron, "SUST_PLU_ADJ_SING", "Adjetivo Singular después de Sust. Plural", excepciones);		
		listaReglas.add(regla);		
//	    -------------------------------------------------------------------------
//		Complemento Regla: Sustantivo seguido de verbo en 1ra. persona
//		DA0FS0¿NCFS000¿VAIP1S0
//		Ejemplo de error: "La casa estoy"
				
//		** ME genera falso positivo. Ejemplo: "El jardin estaba".
		
		
//		patron = Pattern.compile(".*N.{6}¿V.{3}1.{2}.*");
//		regla = new Regla(patron, "", "Sustantivo seguido de verbo en 1ra. persona", excepciones);		
//		reglas.add(regla);		
//	    -------------------------------------------------------------------------
//		Complemento Regla: Sustantivo Singular seguido de verbo plural  
//						   ó Sustantivo Plural seguido de verbo singular (Al inicio oración)		
//		
//		DA0FS0¿NCFS000¿VAII3P0
//		La casa estaban		
//		DA0FS0¿NCFS000¿VSIP3P0
//		La casa son		
		
//		Ejemplo de error: "La casa estaban", "La casa son".
				
		patron = Pattern.compile("(D.{5}¿N.{2}S.{3}¿V.{4}P.{1}.*)|(D.{5}¿N.{2}P.{3}¿V.{4}S.{1}.*)");
		regla = new Regla(patron, "SUST_SING_VERBO_PLURAL", "Sustantivo Singular seguido de verbo plural", excepciones);		
		listaReglas.add(regla);		
//	    -------------------------------------------------------------------------
		
//		Error a detectar: Sustantivo singular + verbo plural ?
//		Ej. "La casa estaban".		
//		Produce falso positivo. Por ejemplo: "Los mecanismos que permiten acelerar y facilitar la escritura deben ser.."
//		"escritura deben" (Es N singular seguido de verbo en plural, pero está bien el plural porque el verbo
//		se relaciona a "mecanismos" y no a escritura.		
//		p = Pattern.compile(".*¿N.{2}S000¿V.{4}P.*");
//		
//		m = p.matcher(oracionTags);		
//		
//		if (m.matches())
//			logReglas.add("Conjugación verbal errónea: " + oracion + "\n");

//      -------------------------------------------------------------------------
		
//		DA0FS0¿NCFS000¿VAIP1S0			-> V Verbo A Auxiliar I Indicativo P Presente 1 1ra. persona * S Singular  
//		La casa estoy						Matchear -> N  con V1 (1ra. persona)		
//		Produce falso positivo. Por ejemplo: La escritura debería ser...
//		En este caso "debería" se conjuga tanto para 1ra. como para 3ra. persona (yo debería, él debería)
		
//		p = Pattern.compile(".*¿N.{2}S.{3}¿V.{3}1.*");
//
//		m = p.matcher(oracionTags);		
//		
//		if (m.matches())
//			logReglas.add("Conjugación verbal errónea 1ra.persona: " + oracion + "\n");
		
//      -------------------------------------------------------------------------		
		
	}

	public List<Regla> getReglas() {
		return listaReglas;
	}
}