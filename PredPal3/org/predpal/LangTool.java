package predpal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.languagetool.AnalyzedSentence;
import org.languagetool.AnalyzedToken;
import org.languagetool.AnalyzedTokenReadings;
import org.languagetool.JLanguageTool;
import org.languagetool.language.Spanish;
import org.languagetool.rules.Rule;
import org.languagetool.rules.RuleMatch;

import predpal.util.Config;

public class LangTool {
	
	private JLanguageTool langTool;
	private List<String> logReglas;
	
	public LangTool() {
		this.langTool = new JLanguageTool(new Spanish());
//		this.logReglas = logReglas;
		logReglas = new ArrayList<String>();
		desactivarReglas();
	}

	private void desactivarReglas() {
		
		List<String> ruleIds = new ArrayList<String>();
				
		ruleIds.add("UPPERCASE_SENTENCE_START");
		ruleIds.add("TU_FINAL");	// org/languagetool/rules/es/grammar.xml
		ruleIds.add("EL_FINAL");	
		ruleIds.add("MI_FINAL");
		ruleIds.add("SE_FINAL");
		ruleIds.add("DE_FINAL");
		ruleIds.add("TE_FINAL");
		ruleIds.add("SOLO");		// Si se ingresa una oración que comienza con "Sólo", tira error y luego no predice nada.
		ruleIds.add("P_V");			// org/languagetool/resource/es/disambiguation.xml 
		ruleIds.add("MORFOLOGIK_RULE_ES");
		ruleIds.add("UNPAIRED_BRACKETS");
		langTool.disableRules(ruleIds);
	}
	
	public JLanguageTool getLangTool() {
		return langTool;
	}
	
	public List<String> verificar(List<String> palPred, String oracionAVerificar)  throws IOException {
		
		List<String> correctas = new ArrayList<String>();
		String oracion = null;		

		for (String palabra : palPred ) {
			
	//		Si el token tiene el punto, no agregarlo.			
			if (oracionAVerificar.length() == 0)
				
				if (palabra.contains("."))
					oracion = palabra;
				else
					oracion = palabra + '.';
			else
				if (palabra.contains("."))				
					oracion = oracionAVerificar + " " + palabra;
				else					
					oracion = oracionAVerificar + " " + palabra + ".";
			
			List<RuleMatch> matches = this.langTool.check(oracion);
	
			if (matches.isEmpty())
				correctas.add(palabra);
	
			for (RuleMatch match : matches) {
	
//				Este if es para que no de error siempre si ingresó
//				una oración que no comienza con mayúscula
//				(Se hace el "+1" por si el token incluye un punto (".") por ejemplo
				if (oracion.length() > (match.getToPos() + 1) ) {
					correctas.add(palabra);
					break;
				}
				
				Rule rule = match.getRule();
	
//				System.out.printf("Rule id.: %s\n", rule.getId());
//				System.out.println(match.getShortMessage());	
//				System.out.println(match.getMessage());			
				
				logReglas.add("Regla: " + rule.getId());
				logReglas.add(match.getMessage());
				logReglas.add(oracion);
				logReglas.add("--------------------------------------------------------------------------");
			}
		}
		return correctas;	
	}
	
	public List<Pos> obtenerPosTags(String oracion, JLanguageTool langTool) throws IOException {

		List<AnalyzedSentence> lista = langTool.analyzeText(oracion);
		AnalyzedSentence as = lista.get(0);
		List<AnalyzedTokenReadings> readings = Arrays.asList(as.getTokens());
		List<Pos> posTags = new ArrayList<Pos>();
		int count = 0, pos = 0;
		
		if (Boolean.valueOf(Config.getProperty("LOG"))) {		
			System.out.println("---------------------------------");
			System.out.println("LangTool:");
			System.out.println("---------------------------------");
		}
		
		for (AnalyzedTokenReadings reading : readings ) {

			pos++;
			
			for ( int i=0; i < reading.getReadingsLength(); i++ ) {

				AnalyzedToken token = reading.getAnalyzedToken(i);	
				
				if (token.getLemma() != null) {
					count++;
					
					if (Boolean.valueOf(Config.getProperty("LOG"))) {					
						System.out.print(reading.getToken() + " - ");
						System.out.print(token.getPOSTag() + " - ");
						System.out.print(i+1 + " - ");
						System.out.println(count);
					}
					
					Pos posReg = new Pos(pos, count, token.getPOSTag(), reading.getToken());

					posTags.add(posReg);	
				}				
			}
			count = 0;
		}
		return posTags;
	}

	public List<String> getLogReglas() {
		return logReglas;
	}
	
	public void borrarLogReglas() {
		this.logReglas.clear();
	}
}