package predpal.test;

import java.util.List;

import predpal.PredPal;

public class Test {

	public Test() {}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		List<String> palabrasPredichas;
		PredPal predPal;
		
		
		predPal = new PredPal();
//		predPal.borrarCategoria("genero");
//		predPal.borrarCategoria("ambito");
//		predPal.entrenarCategoria("e:/datos/ambito.txt", "ambito");
//		predPal.entrenarCategoria("/home/pablo/ambito.txt", "ambito");
//		predPal.entrenarCategoria("/home/pablo/genero.txt", "genero");
//		predPal.entrenarCategoria("e:/datos/test5.txt", "test5");
//		predPal.entrenarCategoria("f:/datos/ambito.txt", "ambito");
//		predPal.entrenarCategoria("e:/datos/el_quijote.txt", "quijote");
//		predPal.entrenarCategoria("e:/datos/test/genero.txt", "genero");
//		predPal.entrenarCategoria("e:/datos/el_quijote.txt", "quijote");
//		predPal.entrenarCategoria("e:/datos/error_comilla.txt", "error_comilla");		
//		predPal.borrarCategoria("test2");
//		predPal.entrenarClasificador("e:/workspace/lingPipe/diarios/train");
//		predPal.entrenarClasificador("/home/pablo/Tesis/Tesis/Datos_entrenamiento/diarios/diarios/train");
//		predPal.entrenarClasificador("e:/Tesis/Datos_entrenamiento/diarios/diarios/train");		
		for ( String categoria : predPal.obtenerCategorias() )
			System.out.println(categoria);	
	

//		predPal.setCategoriaAutomatica(true);
//		System.out.println(predPal.getCategoriaAutomatica());
//		predPal.setCategoria("quijote");
//		predPal.setCategoriaAutomatica(false);
//		System.out.println(predPal.getCategoriaAutomatica());
	
//		predPal.borrarCategoria("quijote");
//		predPal.borrarCategoria("personal");
//		predPal.borrarCategoria("ambito");
//		predPal.borrarCategoria("test5");
//		predPal.borrarCategoria("test6");
//		predPal.borrarCategoria("quijote_short");
//		predPal.borrarCategoria("error_comilla");

		predPal.activarGramatica(true);
		predPal.activarCategoriaPersonal(true);
		predPal.seleccionarCategoria("genero");
		
		System.out.println("grabada:" + predPal.obtenerCategoria());
		predPal.activarGramatica(true);
		palabrasPredichas = predPal.predecir("Las casas son ");
		

//		System.out.println(predPal.obtenerCategoria("diputados"));
		
		for ( String mensaje : predPal.obtenerMensajes() )
			System.out.println(mensaje);
		
		if (palabrasPredichas.isEmpty()) {
			System.out.println("No se encontraron datos");
		} else {
			System.out.println("----------");
			System.out.println("Prediccion");
			System.out.println("----------");
			for ( String palabra : palabrasPredichas )
				System.out.println(palabra);
		}
//		
//		palabrasPredichas = predPal.predecir("candidatos");
//		
//		if (palabrasPredichas.isEmpty()) {
//			System.out.println("No se encontraron datos");
//		} else {
//			System.out.println("----------");
//			System.out.println("Prediccion");
//			System.out.println("----------");
//			for ( String palabra : palabrasPredichas )
//				System.out.println(palabra);
//		}
		
	}
}