package predpal;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

import predpal.util.Config;

/**
 * @author Pablo
 *
 */
public class Texto {

	private String textoEntrada;
	private boolean palabraParcial;

	public Texto(String texto) {
		textoEntrada = texto;

//		Determinar si la ultima palabra ingresada está completa
		setParcial(palabraParcial(texto));
	}
	
	/**
	 * @param ultPal
	 * @param texto
	 * @return
	 */
	private boolean palabraParcial(String texto) {

		char caract;

		caract = (texto.length() > 0) ? texto.charAt(texto.length()-1) : null; 
		
		if (caract != ' ')
			return true;
		else
			return false;
	}

	public String getTextoEntrada() {
		return textoEntrada;
	}

	public void setTextoEntrada(String textoEntrada) {
		this.textoEntrada = textoEntrada;
	}

	public boolean esParcial() {
		return palabraParcial;
	}

	public void setParcial(boolean parcial) {
		this.palabraParcial = parcial;
	}

	public void guardar() {

		OutputStreamWriter fileOut;


		try {
			fileOut = new OutputStreamWriter(new FileOutputStream(Config.getProperty("ARCHIVO_APRENDIZAJE")), Charset.forName("UTF-8").newEncoder());

//			Cargar datos en el archivo
			fileOut.write(this.getTextoEntrada());
			fileOut.close();
		}
		catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		catch (IOException a) {
			System.err.println("Caught IOException: " + a.getMessage());
		}
	}
}