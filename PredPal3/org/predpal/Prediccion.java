/**
 * @author Pablo
 *
 */
package predpal;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import predpal.entrenamiento.ArchEntrada;
import predpal.entrenamiento.ModeloNgram;
import predpal.entrenamiento.Tokenizer;
import predpal.util.Config;
import predpal.util.DBUtil;
import predpal.util.Database;

public class Prediccion {

	private Texto textoEntrada;

	// Cantidad máxima de palabras a obtener de cada método
	private PredPal predPal;
	private static int cantMaxPalabrasPredichas = 20;
	
	/**
	 * Constructor
	 */
	public Prediccion(PredPal predPal) {
		this.predPal = predPal;
	}

	/**
	 * @param ultPal
	 * @return
	 */
	public void obtenerPrediccion(List<String> ultPal, Texto textoEntrada, List<String> palPred, List<String> palPredPersonal) {
		
		List<String> palPredAux = new ArrayList<String>();
		List<String> ultPalAux;
		String palabraLike = "",
			   token = "",
			   sql = "";
				
		
		if (ultPal.isEmpty()) 
//			En este caso no habría que buscar las palabras "Solas" más probables?
			return;
		
		this.textoEntrada = textoEntrada;

//		Copia de ultimas palabras para modificar		
		ultPalAux = new ArrayList<String>(ultPal);		
				
//		Generar una nueva lista, segun si la ultima palabra ingresada es parcial o no
		if (this.textoEntrada.esParcial()) { 
			palabraLike = ultPalAux.get(ultPalAux.size()-1);
			ultPalAux = ultPalAux.subList(0, ultPalAux.size()-1);
		} else {
			int ngram = 0;
			ngram = Integer.valueOf(Config.getProperty("NGRAM")) - 1; 			
			ultPalAux = ultPalAux.subList(Math.max(ultPalAux.size()-ngram, 0), ultPalAux.size());		
		}

//		Repetir la búsqueda segun la cantidad de últimas palabras (viene dado por el tamaño de ngram)
		for ( int i = 0; i < ultPal.size(); i++) {

			for (String palabra : ultPalAux)
				token = token.isEmpty() ? palabra : token + " " + palabra;

//			Se deberían sumar los resultados de ambas búsquedas:

//			Buscar palabras en categoría (Automatica según clasificador o manual)
			sql = generarSql(token, palabraLike, Config.getProperty("CATEGORIA"));
			palPredAux = buscarPalabras(sql);

			agregarPalabras(palPred, palPredAux);

			if (predPal.obtenerCategoriaPersonalActiva()) {
//				Buscar palabras en diccionario personal	
				sql = generarSql(token, palabraLike, "personal");
				palPredAux = buscarPalabras(sql);

				agregarPalabras(palPredPersonal, palPredAux);
			}
			
			if ( (palPred.size()+palPredPersonal.size()) >= Prediccion.cantMaxPalabrasPredichas )
				break;
			else {
//				No se encontraron la cant. suficiente de palabras. Retroceder en el modelo (Backoff).				
				token = "";

				if (ultPalAux.size() > 0)
					ultPalAux = ultPalAux.subList(1, ultPalAux.size());
				else
//					Se retrocedió hasta el final... Salir del loop.
					break;
			}
		}
	}
	
	public String obtenerPalabraParcial(List<String> ultPal) {

		int listSize = ultPal.size();

		return ultPal.get(listSize - 1);
	}

	private String generarSql(String token, String palabraLike, String categoria) {

		String sql;

		if (palabraLike == "") {
			sql = "SELECT palabra FROM ngram " + "WHERE categoria = '" + categoria + "' AND " + "token = '" + token
					+ "'" + " ORDER BY cantidad DESC;";
		} else {
			sql = "SELECT palabra FROM ngram " + "WHERE categoria = '" + categoria + "' AND " + "token = '" + token
					+ "' AND " + "palabra LIKE '" + palabraLike + "%'" + " ORDER BY cantidad DESC;";
		}

		return sql;
	}

	private List<String> buscarPalabras(String sql) {

		List<String> palabras = new ArrayList<String>();
//		Database db = null;
		DBUtil db = null;
		Statement statement;
		ResultSet rs;
		int i = 0;

		try {
//			db = new Database();
			db = new DBUtil();
			statement = db.getConnection().createStatement();

			statement.execute("PRAGMA case_sensitive_like=ON;");
			rs = statement.executeQuery(sql);

			if (!rs.isBeforeFirst()) // No se encontraron datos
				return palabras;

			while (rs.next()) {
				palabras.add(rs.getString("palabra"));
				i = i + 1;

				if (i >= Prediccion.cantMaxPalabrasPredichas)
					break;
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			db.closeDatabase();
		}

		return palabras;
	}

	public void entrenar(String rutaArchivo, String categoria) {

		Tokenizer tokenizer;
		ModeloNgram modelNGram;

		try {
			ArchEntrada arch = new ArchEntrada(rutaArchivo, categoria);

			arch.abrirArchivo();

			tokenizer = new Tokenizer(arch.getBufferedReader());
			tokenizer.generarTokens();

			modelNGram = new ModeloNgram(categoria);
			modelNGram.generarNgramas(categoria, tokenizer.getTokens(), Byte.parseByte(Config.getProperty("NGRAM")));
			modelNGram.guardarNGramas(categoria);

			arch.cerrarArchivo();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<String> obtenerCategorias() {
		ModeloNgram modelNGram;

		modelNGram = new ModeloNgram("");

		return modelNGram.obtenerCategorias();
	}

	public void borrarCategoria(String categoria) {
		ModeloNgram modelNGram;

		modelNGram = new ModeloNgram("");

		modelNGram.borrarCategoria(categoria);
	}

	public void aprender() {

		// Verificar si existe el archivo_learn.txt antes de entrenar:
		File archLearn = new File(Config.getProperty("ARCHIVO_APRENDIZAJE"));

		if (!archLearn.exists())
			return;

		entrenar(archLearn.getName(), "personal");

		// Borrar archivo de aprendizaje.
		archLearn.delete();
		archLearn.exists();

	}

	private void agregarPalabras(List<String> palPred, List<String> palPredAux) {		
		
		for (String palabra : palPredAux) {
			if (!palPred.contains(palabra))
				palPred.add(palabra);
		}

	}
}